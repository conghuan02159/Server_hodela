'use strict';
const suri = require('../models/table_hodela');


exports.mUpView = () =>
    new Promise((resolve, reject) => {


        suri.find({})
            .then(result => {
                if (result.length > 0) {
                    for(var i = 0 ; i < result.length;i++){
                        var rows = result[i];
                        var mView =   rows.view;
                        var random = Math.round(Math.random() * (5 - 1) + 0);
                        rows.view = mView + random;
                        rows.save();
                    }
                    resolve({status: 201, message: "UpView Success With Number:"+random });

                } else {
                    reject({status: 404, message: "Product Not Found !"});
                }


            })

            .catch(err => reject({status: 500, message: "Internal Server Error !"}));

    });